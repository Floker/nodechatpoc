const { Socket } = require('net');
const readline = require('readline').createInterface({
    input: process.stdin,
    output: process.stdout,
});

const END = 'END';

const error = (message) => {
    console.error(message);
    process.exit(1);
}

const connect = (host,port) => {

    const socket = new Socket();
    console.log(`Connecting  to ${host}:${port}`);
    socket.setEncoding('utf-8');
    socket.connect({ host, port});

    socket.on("connect", () => {
        console.log(`Connected successfully`);

        readline.question("Choose your nick: ",(nickname) => {
            socket.write(nickname);
        });

        readline.on("line", (message) => {
            socket.write(message);
            if (message === END) {
                socket.end();
                console.log("Disconnected");
            }
        });
        
        socket.on("data", (data) => {
            console.log(data);
        });
    });

    socket.on("error", (err) => error(err.message));
    socket.on("close", () => process.exit(0));

}




const main = () => {
    if(process.argv.length !== 4){
        error(`Usage: node ${__filename} host port`);
    }

    let host = process.argv[2];
    let port = process.argv[3];
    
    connect(host,port);
}

if (require.main === module) {
    main();
}